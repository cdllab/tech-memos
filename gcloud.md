# GCloud Memo

## Configuration

See: [gcloud config set](https://cloud.google.com/sdk/gcloud/reference/config/set).

Configration is stored in `~/.config/gcloud/configurations/config_default`.

## Instances

Stop and delete instance:

```sh
gcloud --impersonate-service-account=<service-account> compute instances stop <instance-name>
gcloud --impersonate-service-account=<service-account> compute instances delete <instance-name>
```
