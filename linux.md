# Linux Memo

## References

- [Bash scripting cheatsheet](https://devhints.io/bash).
- [$(...) preferred over `...` (backticks)](https://mywiki.wooledge.org/BashFAQ/082).

## Linux file system

- [Linux folder tree](https://i.stack.imgur.com/3u7dC.jpg).
- [Use of `/usr/local` and `/opt`](https://unix.stackexchange.com/a/11552):

> While both are designed to contain files not belonging to the operating system, `/opt` and `/usr/local` are not intended to contain the same set of files.
>
> `/usr/local` is a place to install files built by the administrator, typically by using the make command (e.g., `./configure`; `make`; `make install`). The idea is to avoid clashes with files that are part of the operating system, which would either be overwritten or overwrite the local ones otherwise (e.g., `/usr/bin/foo` is part of the OS while `/usr/local/bin/foo` is a local alternative).
>
> All files under `/usr` are shareable between OS instances, although this is rarely done with Linux. This is a part where the FHS is slightly self-contradictory, as `/usr` is defined to be read-only, but `/usr/local/bin` needs to be read-write for local installation of software to succeed. The SVR4 file system standard, which was the FHS' main source of inspiration, is recommending to avoid `/usr/local` and use `/opt/local` instead to overcome this issue.
> 
> `/usr/local` is a legacy from the original BSD. At that time, the source code of `/usr/bin` OS commands were in `/usr/src/bin` and `/usr/src/usr.bin`, while the source of locally developed commands was in `/usr/local/src`, and their binaries in `/usr/local/bin`. There was no notion of packaging (outside tarballs).
> 
> On the other hand, `/opt` is a directory for installing unbundled packages (i.e. packages not part of the Operating System distribution, but provided by an independent source), each one in its own subdirectory. They are already built whole packages provided by an independent third party software distributor. Unlike `/usr/local` stuff, these packages follow the directory conventions (or at least they should). For example, someapp would be installed in `/opt/someapp`, with one of its command being `/opt/someapp/bin/foo`, its configuration file would be in `/etc/opt/someapp/foo.conf`, and its log files in `/var/opt/someapp/logs/foo.access`.

## Terminal



Edit `~/.inputrc` to set terminal bell sound:



```sh

set bell-style none

#set bell-style visible

#set bell-style audible

```



Set bash to show the list of matching commands/items immediately when you hit _tab_ (`~/.inputrc`):cd 



```sh

set show-all-if-ambiguous on

```



## source



- `source <script>` - fork the current shell to execute `<script>`.

- `. <script>` - execute `<script>` in the current shell.



When running a script inside another script, use the dot (`.`) to preserve the environment.



## ls



- `-h` - display size in human readable format.

- `-S` - sort by size, largest first.

- `-t` - sort by modification time, newest first.

- `-X` - sort by extension.

- `-u`:

    - with `-lt`: sort by, and show, access time;

    - with `-l`: show access time and sort by name;

    - otherwise: sort by access time, newest first.

- `-r` - reverse order while sorting.



## Tail



- `tails -f <filename> | grep --line-buffered <pattern>` - filter `tail -f` output with grep.

- `cat -n <file> | tail -n +<line> | head -n 11` - display 5 lines before and after `<line> + 5` with line numbers. Don't forget the `+` in `tail` command.



## Watch directory



```sh

sudo apt-get install inotify-tools

```



```sh

inotifywait -m -e create -e moved_to --format "%f" <folder/to/watch> | while read FILENAME; do echo $FILENAME; done

inotifywait -m -e create -e moved_to --format "%f" <folder/to/watch> | while read FILENAME; do stat -c "%y %s %n" <folder/to/watch>/$FILENAME; done

```



## Human readable size



From GNU `coreutils`:



```sh

$ numfmt --to=iec-i --suffix=B --format="%.3f" 4953205820

4.614GiB

```



## Find



[35 Practical Examples of Linux Find Command](https://www.tecmint.com/35-practical-examples-of-linux-find-command/).



Find in files with `grep`:



```sh

find <start-dir> -type f -name <filename> -exec grep -Hi <pattern> {} +

```



### Options



- `-type f|d` - find only files (`f`) or directory (`d`).



## Permission



Change directory|file only permission:



```sh

find /path/to/base/dir -type d -print0 | xargs --no-run-if-empty -0 chmod 755 

find /path/to/base/dir -type f -perm /u=x -print0 | xargs --no-run-if-empty -0 chmod 755

find /path/to/base/dir -type f ! -perm /u=x -print0 | xargs --no-run-if-empty -0 chmod 644

```



find /opt -type f -perm /u=x -print0 | xargs --no-run-if-empty -0 chmod 755 && find /opt -type d -print0 | xargs --no-run-if-empty -0 chmod 755 && find /opt -type f ! -perm /u=x -print0 | xargs --no-run-if-empty -0 chmod 644



## Apt Packages



`apt` log file is: `/var/log/apt/term.log`.



Clean lock state:



```sh

ps aux | grep -i apt

sudo kill -9 <pid>



sudo rm -f /var/lib/dpkg/lock

sudo rm -f /var/lib/dpkg/lock-frontend

sudo rm -f /var/lib/apt/lists/lock

sudo rm -f /var/cache/apt/archives/lock



# get the process that is holding a lock on config.dat

sudo fuser -v /var/cache/debconf/config.dat

sudo kill -9 <pid>



sudo dpkg --configure -a

```



[Ubuntu package repositories explained](https://itsfoss.com/ubuntu-repositories/):



> Software in Ubuntu repository are divided into five categories: main, universe, multiverse, restricted and partner:



> - **Main** - this is the repository enabled by default. The main repository consists of only FOSS.

> - **Universe** - this repository also consists free and open source software but Ubuntu doesn’t guarantee of regular security updates to software in this category.

> - **Multiverse** - contains the software that are not FOSS.

> - **Restricted** - the restricted repositories consists of proprietary drivers.

> - **Partner** - this repository consist of proprietary software packaged by Ubuntu for their partners.



## Sudo



Edit sudo configuration file:



```sh

sudo visudo

```



Update `sudo` group config with no password for sudoers:



```sudo

%sudo	ALL=(root) NOPASSWD: ALL

```



Add user to `sudo` group:



```sh

sudo usermod -aG sudo <username>

```



## Special Variables



- `$!` - expands to the process ID of the most recently executed background (asynchronous) command. `<command> & pid=$!` store process ID of background command in variable `pid`.

- `$#` - stores the number of command-line arguments that were passed to the shell program.

- `$0` - stores the command name.

- `$*` - stores all the arguments that were entered on the command line in a single string.

- `$@` - stores all the arguments that were entered on the command line in an array.

- `$?` - stores the exit value of the last command that was executed.



## Zip



`zip -r <archive>.zip file...`.



Exclude a specific folder with `-x "*<folder_name>/*"`.



## Network



Ping a specific port (using netcat):



```sh

nc -vz <address> <port>

```



- `-z`- specifies that `nc` should just scan for listening daemons, without sending any data to them.

- `-v` - verbose mode.



## SSH



Run a local script on the remote host:



```sh

ssh <server> "bash -s" < ./my_script.sh



# if need to pass arguments to script

ssh <server> "bash -s" -- < ./my_script.sh "arg1" "arg2"

```



## Performance



1. Check if on the right system: `hostname`.

1. Check disk space: `df`, `du`.

1. Check processing: `top`, `free`, `lsmem`...

1. Check disk issues: `iostat`, `lsof`.

1. Check networking: `tcpdump`, `ns`, `ss`, `ìftop`, `lsof`...

1. System uptime: `uptime`.

1. Check logs.

1. Check hardware status.

1. Other tools: `htop`, `iotop`, `iptraf`, `psacct`..



```sh

hostname



# check disk usage percentage

df -h

# check file spaces

du -ah / | sort -nr | less

# disk stats report every 5s 

iostat -y 5



# check processes status

top



# memory

free -m

lsmem

cat /proc/meminfo

# virtual memory

vmstat

# memory info for process pid

pmap <pid>



# hardware information

dmidecode



# CPU architecture

lscpu

cat /proc/cpuinfo

```



GNU `time` command (`/usr/bin/time`) reports process time (user, system, total). 



> **Real, User and Sys process time statistics**

>

> One of these things is not like the other. Real refers to actual elapsed time; User and Sys refer to CPU time used only by the process.

>

> - **Real** is wall clock time - time from start to finish of the call. This is all elapsed time including time slices used by other processes and time the process spends blocked (for example if it is waiting for I/O to complete).

> - **User** is the amount of CPU time spent in user-mode code (outside the kernel) within the process. This is only actual CPU time used in executing the process. Other processes and time the process spends blocked do not count towards this figure.

> - **Sys** is the amount of CPU time spent in the kernel within the process. This means executing CPU time spent in system calls within the kernel, as opposed to library code, which is still running in user-space. Like 'user', this is only CPU time used by the process. See below for a brief description of kernel mode (also known as 'supervisor' mode) and the system call mechanism.

>

> **User+Sys** will tell you how much actual CPU time your process used. Note that this is across all CPUs, so if the process has multiple threads (and this process is running on a computer with more than one processor) it could potentially exceed the wall clock time reported by Real (which usually occurs). Note that in the output these figures include the User and Sys time of all child processes (and their descendants) as well when they could have been collected, e.g. by wait(2) or waitpid(2), although the underlying system calls return the statistics for the process and its children separately.



Reference: https://stackoverflow.com/a/556411



## Various Commands



- `mkfifo <pipe_name>` - create named pipe.

- `dirname <file_name>` - strip non-directory suffix from file name.

- `readlink` - print value of a symbolic link or canonical file name:

    - `-f`, `--canonicalize`: canonicalize by following every symlink in every component of the given name recursively; all but the last component must exist.

- `set -e` - causes the shell to exit if any subcommand or pipeline returns a non-zero status (exit immediately when a command fails).



    > Note: Be careful of using `set -e` in `init.d` scripts.

    > Writing correct `init.d` scripts requires accepting various error exit statuses when daemons are already running or already stopped without aborting the `init.d` script, and common `init.d` function libraries are not safe to call with `set -e` in effect.

    > For `init.d` scripts, it's

     often easier to not use `set -e` and instead check the result of each command separately.



- `set -o pipefail` - sets the exit code of a pipeline to that of the rightmost command to exit with a non-zero status, or to zero if all commands of the pipeline exit successfully.

- `set -x` - display variable expansion and command as the script is running.

- `wait <pid>...`- built-in command that waits for completing any running process. If no process id or job id is given with `wait` command then it will wait for all current child processes to complete and returns exit status.


        
